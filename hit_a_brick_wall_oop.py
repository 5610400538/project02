import sys, pygame, time, random

class Ball:
    speed = (0,8)
    def __init__(self,pic):
        self.surface = pygame.image.load(pic).convert_alpha()
        self.ball = self.surface.get_rect()
    
class Brick:
    
    def __init__(self,pic):
        self.surface = pygame.image.load(pic).convert_alpha()
        self.brick = self.surface.get_rect() 
        
    def setup_brick(self,brick,scorebrick,lifebrick):
        bw, bh = brick.size
        Game.brick_wall.clear()
        for i in range(104):
            if i == 58:
                Game.brick_wall.append(lifebrick.move((i%13)*bw+40, 80+i//13*bh))
            if i in [44,45,46,57,59,70,71,72]:
                Game.brick_wall.append(scorebrick.move((i%13)*bw+40, 80+i//13*bh))
            if i not in [44,45,46,57,58,59,70,71,72]:
                Game.brick_wall.append(brick.move((i%13)*bw+40, 80+i//13*bh)) 
                
    def setup_bricks2(self,brick,scorebrick,lifebrick,hardbrick):
        bw, bh = brick.size
        Game.brick_wall.clear()
        for i in range(104):
            if i == 58:
                Game.brick_wall.append(lifebrick.move((i%13)*bw+40, 80+i//13*bh))
            if i in [44,45,46,57,59,70,71,72]:
                Game.brick_wall.append(scorebrick.move((i%13)*bw+40, 80+i//13*bh))
            if (i not in [44,45,46,57,58,59,70,71,72]) and (i%2 != 0):
                Game.brick_wall.append(brick.move((i%13)*bw+40, 80+i//13*bh))
            if (i not in [44,45,46,57,58,59,70,71,72]) and (i%2 == 0):
                Game.brick_wall.append(brick.move((i%13)*bw+40, 80+i//13*bh))
                Game.brick_wall.append(hardbrick.move((i%13)*bw+40, 80+i//13*bh))
                
    def setup_bricks3(self,brick,scorebrick,lifebrick,hardbrick):
        bw, bh = brick.size
        Game.brick_wall.clear()
        for i in range(104):
            if i == 58:
                Game.brick_wall.append(lifebrick.move((i%13)*bw+40, 80+i//13*bh))
            if i in [44,45,46,57,59,70,71,72]:
                Game.brick_wall.append(scorebrick.move((i%13)*bw+40, 80+i//13*bh))
            if (i not in [44,45,46,57,58,59,70,71,72]):
                Game.brick_wall.append(brick.move((i%13)*bw+40, 80+i//13*bh))
                Game.brick_wall.append(hardbrick.move((i%13)*bw+40, 80+i//13*bh)) 

class Paddle:
    speed = 25
    def __init__(self,pic):
        self.surface = pygame.image.load(pic).convert_alpha()
        self.paddle = self.surface.get_rect()  
        
class Score_brick(Brick):
    def __init__(self,pic):
        Brick.__init__(self,pic)
        
class Life_brick(Brick):
    def __init__(self,pic):
        Brick.__init__(self,pic)
        
class Hard_brick(Brick):
    def __init__(self,pic):
        Brick.__init__(self,pic)
        

                
class Game:
    brick_wall = []
    def __init__(self):
        self.bg_color = (80, 60, 0)
        self.ticks = 80
        
        pygame.mixer.pre_init(44100, -16, 1, 512)
        pygame.init()
        pygame.key.set_repeat(1, 30)
        pygame.mouse.set_visible(0)
        
        self.size = self.width, self.height= (800,700)    
        self.screen = pygame.display.set_mode(self.size)
        pygame.display.set_caption("Hit A Brick Wall")  
        
        self.hit_paddle_sound = pygame.mixer.Sound('hitDown.wav')
        self.hit_brick_sound = pygame.mixer.Sound('199.wav')
        self.hit_top_sound = pygame.mixer.Sound('hitUp.wav')
        self.end_sound = pygame.mixer.Sound('theEnd.wav')
        self.bg_sound = pygame.mixer.Sound('bgs.wav')
        self.vic_sound = pygame.mixer.Sound('victory.wav')
        self.over_sound = pygame.mixer.Sound('gameover.wav') 
        
        self.ball = Ball("ball.png")
        self.brick = Brick("brick.png")
        self.paddle = Paddle("paddle.png")
        self.lbrick = Life_brick("lifebrick.png")
        self.sbrick = Score_brick("20brick.png")
        self.hbrick = Hard_brick("hbrick.png")
        self.bg_surface = pygame.image.load("bg.png").convert()
        self.bg = self.bg_surface.get_rect()
        
        self.paddle.paddle.move_ip(self.width/2 - self.paddle.paddle.width/2, self.height - 25)
        self.ball.ball.move_ip(self.width/2 - self.ball.ball.width/2, self.height/2)        

    def start(self):
        self.score = 0
        self.life = 5
        self.level = 1
        self.clock = pygame.time.Clock()  
        
        self.move_ball_x,self.move_ball_y = Ball.speed
        self.move_paddle = Paddle.speed
        
        self.brick.setup_brick(self.brick.brick, self.sbrick.brick, self.lbrick.brick)
        
        self.bg_sound.play(10)
        while True:
            self.clock.tick(self.ticks)
            for event in pygame.event.get():
                self.event_handler(event)
            if self.paddle.paddle.bottom-self.move_ball_y >= self.ball.ball.bottom >= self.paddle.paddle.top and \
                self.paddle.paddle.right >= self.ball.ball.centerx >= self.paddle.paddle.left:
                if self.ball.ball.centerx < self.paddle.paddle.right  and self.ball.ball.centerx > self.paddle.paddle.centerx:
                    self.move_ball_x = random.randint(2,7)
                if self.ball.ball.centerx > self.paddle.paddle.left and self.ball.ball.centerx < self.paddle.paddle.centerx:
                    self.move_ball_x = -(random.randint(2,7))
                if self.ball.ball.centerx == self.paddle.paddle.right:
                    self.move_ball_x = 8
                if self.ball.ball.centerx == self.paddle.paddle.left :
                    self.move_ball_x = -8
                if self.ball.ball.centerx == self.paddle.paddle.centerx:
                    self.move_ball_x = random.randint(-1,1)
                self.move_ball_y = -self.move_ball_y 
                self.hit_paddle_sound.play(0)

            self.ball.ball.move_ip(self.move_ball_x, self.move_ball_y)
            if self.ball.ball.top > self.height :
                self.end_sound.play(0)
                self.life -= 1
                self.ball.ball.top = 350
                self.ball.ball.right = 424
                self.move_ball_x = 0
                self.move_ball_y = 0
            if self.life == -1 :
                self.bg_sound.stop()
                self.over_sound.play(0)
                self.blink_text("Continue? (y/n)")
            if len(Game.brick_wall) ==0 :
                self.level += 1
                if self.level == 2:
                    self.brick.setup_bricks2(self.brick.brick ,self.sbrick.brick,self.lbrick.brick,self.hbrick.brick)
                    self.ball.ball.top = 350
                    self.ball.ball.right = 424
                    self.move_ball_x = 0
                    self.move_ball_y = 0 
                if self.level == 3:
                    self.brick.setup_bricks3(self.brick.brick ,self.sbrick.brick,self.lbrick.brick,self.hbrick.brick)
                    self.ball.ball.top = 350
                    self.ball.ball.right = 424
                    self.move_ball_x = 0
                    self.move_ball_y = 0  
                if self.level == 4:
                    self.bg_sound.stop()
                    self.vic_sound.play(0)
                    self.blink_text("You win!!! (Try again(y/n)")

            if self.ball.ball.top < 0:    # The ball hits the upper side of the window
                self.move_ball_y = -self.move_ball_y
                self.hit_top_sound.play(0)
        
            if self.ball.ball.left<0:
                self.move_ball_x = -self.move_ball_x
                self.hit_top_sound.play(0)
        
            if self.ball.ball.right>self.width:
                self.move_ball_x = -self.move_ball_x
                self.hit_top_sound.play(0)
        
            if self.move_ball_x==0 and self.move_ball_y==0 :
                if self.paddle.paddle.centerx == 400 or self.paddle.paddle.centerx == 412 or self.paddle.paddle.centerx == 413:
                    self.move_ball_y = 8

            self.index = self.ball.ball.collidelist(Game.brick_wall)
            if self.index >= 0:
                
                if self.ball.ball.top < Game.brick_wall[self.index].bottom or self.ball.ball.bottom > Game.brick_wall[self.index].top:
                    if self.ball.ball.centerx > Game.brick_wall[self.index].left and self.ball.ball.centerx < Game.brick_wall[self.index].right:
                        self.move_ball_y = -self.move_ball_y
                    else:
                        self.move_ball_x = -self.move_ball_x
                
    
                    if list(Game.brick_wall[self.index])[2:4]==[54,18] :
                        self.life += 1
    
                
                    if list(Game.brick_wall[self.index])[2:4]==[55,17]:
                        self.score += 20
                    else:
                        self.score += 10
                    
                    if self.index+1 < len(Game.brick_wall):    
                        if list(Game.brick_wall[self.index+1])[2:4]==[56,18] :
                            Game.brick_wall.pop(index+1)
                        else:
                            Game.brick_wall.pop(self.index)
                    else:
                        Game.brick_wall.pop(self.index)                


                self.hit_brick_sound.play(0)

            self.screen.fill(self.bg_color) 
            self.screen.blit(self.bg_surface, self.bg)

            for b in Game.brick_wall:
                if list(b)[2:4] == [54,18]:
                    self.screen.blit(self.lbrick.surface, b)
                if list(b)[2:4] == [55,17]:
                    self.screen.blit(self.sbrick.surface, b)
                if list(b)[2:4] == [55,18]:
                    self.screen.blit(self.brick.surface, b)
                if list(b)[2:4] == [56,18]:
                    self.screen.blit(self.hbrick.surface, b)

            self.screen.blit(self.ball.surface, self.ball.ball)       
            self.screen.blit(self.paddle.surface, self.paddle.paddle)

            self.text("Score : "+str(self.score) , (70,20))   
            self.text("Life left : "+str(self.life),(235,20))
            self.text("Level : "+str(self.level),(400,20))

            pygame.display.flip()


        
    def blink_text(self,text):
        buffer = pygame.Surface(pygame.display.get_surface().get_size())
        buffer.blit(pygame.display.get_surface(), (0,0))

        font = pygame.font.Font(None,70)
        text_surface = font.render(text, True, (0,255,0), self.bg_color)
        surface = pygame.Surface(font.size(text))
        surface.blit(text_surface, (0, 0))
        surface.set_colorkey(self.bg_color)
        text_rect = text_surface.get_rect()
        text_rect.move_ip(self.width/2 - text_rect.centerx, 0.4*self.height)
        dir, alpha = -5, 255
        while True:
            self.clock.tick(self.ticks)
            for event in pygame.event.get():
                self.event_handler(event)
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_y:
                        self.start()
                    if event.key == pygame.K_n:
                        pygame.quit()
                        sys.exit()
            pygame.display.get_surface().blit(buffer,(0,0))
            surface.set_alpha(alpha)
            self.screen.blit(surface, text_rect)
            pygame.display.flip()
            alpha += dir
            if alpha in [0, 255]:
                dir = -dir
                
    def text(self,text,pos):
        message = pygame.font.Font(None,36)
        message_surface = message.render(text, True, (255,0,0), self.bg_color)
        messagesurface = pygame.Surface(message.size(text))
        messagesurface.blit(message_surface, (0, 0))
        messagesurface.set_colorkey(self.bg_color)
        message_rect = message_surface.get_rect(center=pos)
        self.screen.blit(messagesurface,message_rect )        
        
    def event_handler(self,event):
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            if event.key in [pygame.K_ESCAPE, ord('q'), ord('Q')]:
                pygame.quit()
                sys.exit()
            elif event.key == pygame.K_LEFT:
                self.paddle.paddle.move_ip(-self.move_paddle, 0)
                if self.paddle.paddle.left < 0:
                    self.paddle.paddle.left = 0
            if event.key == pygame.K_RIGHT:
                self.paddle.paddle.move_ip(self.move_paddle, 0)
                if self.paddle.paddle.right > self.width:
                    self.paddle.paddle.right = self.width  

if __name__=="__main__":
    game=Game()
    game.start()
